# README

Rails with react from Deanin:
https://www.youtube.com/watch?v=yoLJXjEV2nM

First, create the rails app:

```
npm install --global yarn
rails new rails_react -j esbuild
cd rails_react
```

here you can see it also created a package.json with some more stuff in it;

You can run `bin/dev` to run the server

Now, create a stimulus and the controller:

```
rails g stimulus react
rails g controller pages home
```

Just change the root to the pages#home:

```diff
-  get 'pages/home'
+  root 'pages#home'
```

Now we can see the home page with the contents:

```h
<h1>Pages#home</h1>
<p>Find me in app/views/pages/home.html.erb</p>
<%= content_tag(:div, "", id: "app", data: {
  controller: "react"
}) %>
```

And the `react_controller.js` stimulus:

```js
import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="react"
export default class extends Controller {
  connect() {
    console.log("React controller connected!")
  }
}
```

Then start adding react functionality:

```
import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="react"
export default class extends Controller {
  connect() {
    console.log("React controller connected!")
    const app = document.getElementById("app")
    ReactDOM.render(<App />, app)
  }
}
```

Add React DOM:

```
yarn add react react-dom
```

Add the `app/javascript/components/App.jsx` component:
After some refactoring, we got a simple app that boils down to a counter. First the `react_controller.js`:

```js
import { Controller } from "@hotwired/stimulus"
import React from "react"
import { createRoot } from "react-dom/client"
import App from "../components/App"

// Connects to data-controller="react"
export default class extends Controller {
  connect() {
    console.log("React controller connected!")
    const app = document.getElementById("app")
    createRoot(app).render(<App />)
  }
}
```

and the `App.jsx`:

```jsx
import React, { useState } from "react"

function App() {
  const [count, setCount] = useState(0)

  return (
    <div>
      <p>You clicked {count} times!</p>
      <button onClick={() => setCount(count + 1)}>Click me to add!</button>
    </div>
  )
}

export default App
```
