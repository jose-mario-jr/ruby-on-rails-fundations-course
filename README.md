# This course is to learn the fundamentals of Ruby on Rails

Here are the course notes, divided by section

# Section 1 (19m): Introduction and Installing Rails

## 1. What is Ruby on Rails?

A framework for web development that doesn't hurt. Optimized for programmer happiness

## 2. How to Install Rails on a PC

Just follow along, the goal is to have `ruby -v` and `rails -v` have an output

## 3. How to Install Rails on a Mac

Just follow along, the goal is to have `ruby -v` and `rails -v` have an output

## Test 1: Rails Introduction Quiz

1. What is Rails?
   R: A web Framework
2. What are the preferred operating systems for Rails development
   R: Mac and Linux

# Section 2 (73m): Building your first Rails Application

## 4. How to Create a Ruby on Rails Application

```
rails new taski
```

Note: I had to remove the `.git` file it generated because I wanted to use it in my repo with all rails study projects instead of one for taski

## 5. Explore the Different Rails App Creation Options

```
rails new --help
```

Analyze that and create an example would be that:

```
rails new taski --database=postgresql --skip-test-unit --skip-turbolinks
```

Note: I didn't find skip-test-unit and skip-turbolinks

## 6. How to Run the Rails Server and View it in the Browser

```
rake db:create:all && rake db:migrate
```

This setup our Postgres and now we can run:

```
rails s
```

## 7. Explore the List of Database Rake Tasks

A lot of rake db:... commands, some to create, some to migrate, reset, etc...
The following link may be a starting point:

- https://guides.rubyonrails.org/v4.2/command_line.html

You can always do the help command:

```
rake --tasks
```

## 8. How to Use Sublime Text as a Text Editor in Rails

The teacher is using sublime, but I'm using VsCode.
I installed the `Ruby` and `Ruby on Rails` extensions

## 9. Exploring the File System of a Ruby on Rails Application - Part 1

Went through the first part.
It is important to note that we're in different versions of Ruby, mine is `7.0.4.3` and he is `4.2.3`, so the structure is very different.
I stopped at this point to understand the structure of my project.

## 10. Exploring the File System of a Ruby on Rails Application - Part 2

Important to note that all information is in the comments of the files

## 11. Should you Use Scaffolds or Generators?

Scaffolds may create useless code, and as more advanced I become, I start reducing useless code in the projects, making it more minimalistic.

## 12. Creating Your First Rails Scaffold

```
rails g scaffold Project title:string description:text percent_complete:decimal
```

And then migrate:

```
rake db:migrate
```

## 13. Reviewing What Scaffolds Create

Traversed all the code, need to review it again later.

It basically created the whole crud

## Test 2: Quiz on Building Your First Rails Application

1. What is the command to create a new Rails application?
   R: rails new
2. What would be the correct way to have Rails automatically use Postgres as a database?
   R: rails new --database=postgresql
3. What web address do you go to in order to view the app?
   R: localhost:3000
4. What directory stores your CSS and JavaScript assets?
   R: app
5. What directory stores your `routes.rb` file?
   R: config
6. Why wouldn't you always use a scaffold?
   R: Scaffolds can create code that isn't needed
7. What command do you use to create a Rails scaffold?
   R: rails g scaffold
8. What functionality do scaffolds give to your application?
   R: CRUD functionality
9. What database rake task will clear out the database and run the `seeds.rb` file?
   R: rake db:setup

# Section 3 (24m): Rails Console

1. How do you start up the Rails console?
   R: rails c
2. The Rails console lets you run queries from the database.
   R: True
3. What command would we use to find the most recent Project added to the database?
   R: `Project.last`
4. Does the `Model.where()` return a single item or a group of items?
   R: Group of items
5. What library is used to run queries in the console?
   R: ActiveRecord

## 14. Introduction to the Rails Console

To enter the console:

```
rails c
```

To enter it in sandbox:

```
rails c --sandbox
```

It will rollback any modifications done on the database when we exit

## 15. How to Create Records in the Rails Console

first enter console:

```
rails c
```

then create 10 projects:

```
irb(main):007:1* 10.times do |project|
irb(main):008:1*   Project.create!(title: "Project #{project}", description: "My cool description")
irb(main):009:0> end
```

## 16. How to Update and Delete Records in the Rails Console

First, open the console:

```
rails c
```

show all projects, count, last, etc...:

- `Project.all`: Show all projects;
- `Project.count`: Show how many projects I have;
- `Project.last`: The last project;

Example of update manually:

first put the last project in a variable, then update it:

```
p = Project.last
p.update!(title: "My cool title")
```

## 17. Advanced Database Queries in the Rails Console

In the console, we can do:

- `Project.find(5)`: get one project of id 5;

- `Project.find([5, 3, 6])`: Get an array of projects in the order of the parameters;
  - Important to focus that it's an array instead of only one, so we can't access. However, we can do with `each`:
    - ```
      Project.find([5, 3, 6]).each do |e|
        puts e.description
      end
      ```
  - You can also use it this way that it works too:
    - `Project.find(5, 3, 6)`
- We can also change properties:
  - `Project.find(5).title.upcase` will make the title uppercase
  - `Project.find(5,3).last.title.upcase` Does the same with the one on item 3
- Conditionals:
  - `Project.where(title: "Project 2")`
  - `Project.where.not(title: "Project 2")`

## Test 3: Rails Console Quiz

Completed

# Section 4 (29m): Routing in Rails

## 18. Introduction to Routes in Ruby on Rails

Explains the routes in the browser

## 19. RESTful Routing in Rails

Gives the command to show all the routes in the terminal

```
# The original from the course
rake routes
# for rails 6 and above:
rails routes
```

## 20. How to Create a Custom Controller in Rails

```
rails g controller Pages contact about home
```

And edit the about.html.erb

## 21. How to Create Custom Routes for Non CRUD Pages

In routes.rb we can change the line:

```diff
-  get 'pages/about'
+  get 'about', to: "pages#about"
```

to access the pages without having to go to `/pages/about`

## 22. How to Set the Homepage for a Rails Application

Just add on the routes:

```
  root 'pages#home'
```

Also changed the homepage to show the projects with the code in pages_controller:

```
  def home
    @projects = Project.all
  end
```

and this one in `home.html.erb`:

```
<% @projects.each do |project| %>
  <p><strong><%= project.title %></strong></p>
<% end %>
```

## 23. How to Integrate Routing Redirects in Rails

Basically showing the way to do redirects, relative and absolute paths

## Test 4: Rails Routing Quiz

1. Where is the routes.rb file stored in the application?
   R: config/
2. What Rails shortcut includes all seven CRUD routes in a single word?
   R: resources
3. Where should the 'catch all' route be placed in the route's file?
   R: At the end of the file
4. In the syntax: "pages#home", what does each word stand for?
   R: "pages" is the controller, "home" is the method
5. How do you setup a route that redirects to another webpage?
   R: get "blog", to: redirect("http://jordanhudgens.com")

# Section 5 (40m): Configuring Views

## 24. Overview of the Master Application Layout File

Add a link to the main file:

```
    <%= link_to 'Contact', contact_path %>
```

## 25. How to Use View Partials

Create \_nav to use it in a neat way and \_projects too

## 26. Advanced ERB Tips

- Comment out embedded ruby:
  - `<strong><%#= project.title %></strong>`
- Conditionals too:
  - `<%= "| #{project.percent_complete}%" if project.percent_complete %>`

## 27. How to Integrate Images into a Rails Application

Just add image tag

## 28. How to Integrate Custom CSS Styles Using the Rails Asset Pipeline

Basically add in the css file

## 29. Integrating Web Safe Fonts into a Rails Application

Just look for the compatible and add the css

## 30. How to Integrate a Custom Font into a Rails Application

First download from [DaFont](https://www.dafont.com/pt/), unzip it, add into the fonts and in application.rb:

```ruby
config.assets.enabled = true
config.assets.paths << Rails.root.join('/app/assets/fonts')
```

```css
theninthecss: @font-face {
  font-family: "Lost Castedral";
  src: url("/assets/Lost_Castedral");
}
h1 {
  font-family: "Lost Castedral", "Arial Black", Gadget, sans-serif;
}
```

note that we don't add a extension on the font, because it doesn't matter. The teacher used a ttf and I used a otf, but it didn't changed the implementation

## Test 5: Rails Views Quiz

1. Which directory is the master application layout file stored?
   R: app/views/layouts/
2. How is <% code... %> different from <%= code... %> ?
   R: <%= code... %> renders the code to the screen, <% code... %> doesn't show anything in the browser
3. What is the difference between a web safe font and an external font?
   R: Web safe fonts are compatible with most browsers and don't need to be imported, external fonts need to be imported manually
4. What directory stores the asset pipeline files?
   R: app/assets/
5. What helper method lets you integrate images into an application using the Rails pipeline?
   R: image_tag()

# Section 6 (26m): Rails Controllers

## 31. Purpose of Controllers in Rails

First explains that ProjectsController inherits from ApplicationController and that if we create a method `foo` in ApplicationController, the ProjectsController has access to the method.

Explain the before_action and some controller properties: not very clear

## 32. Learn What Methods in Controllers Do

- Some features such as `format`, for html and json;
- Explains the `notice` attribute;
- Explains the `project_params` function;

## 33. Stay Away from These Controller Antipatterns

Avoid big controllers (300 lines is tooooo much!!!)

> You will want to have `one` class do `one` thing!

Projects controller has the goal to setup dataflow between the view and the model for the project site

He shows a big controller that doesn't do just that, and it does a lot of stuff out of it's scope:

> If you're starting to put a ton of code inside your controller, really start trying to move those items from the controller into the model file.

## 34. Integrating Custom Queries in Rails Controllers

Explains how we can query in the index method,
For example, we can have the projects without specific name and limited to 5:

```ruby
  # GET /projects or /projects.json
  def index
    @projects = Project.where.not(title: "Project 1").limit(5)
  end
```

We can also have for certain user:

```ruby
  # GET /projects or /projects.json
  def index
    @projects = Project.where(user_id: current_user.id)
  end
```

This logic is nice, we should be wary of that:

- if it gets too big it's better to just move into the model file;

For me personally, it should be moved right away. It can look like overengineering, but we know we'll do that anyways, so what's the harm?

## Test 6: Rails Controllers Quiz

1. How do controllers pass data to the views?
   R: Through Instance variables

2. Where should complex code be placed in a Rails application?
   R: The model.

# Section 7 (44m): Rails Models

## 35. Purpose of Models in Rails

Here we insert the overall logic of the program:

- Custom scopes;
- Add defaults;
- Integrate:
  - validations;
  - callbacks;
  - database relationships.

## 36. Creating Custom Model Files for Algorithm Integration

Created the `counter.rb` and the method `calculate_percent_complete` for it, then called from `rails c`

## 37. How to Integrate Custom Database Scopes in a Rails Model File

To add a scope you just need to add into the model file, for example:

```rb
  scope :still_needs_some_work, -> { where('percent_complete < 75.0') }
```

And then call in the controller:

```rb
    @projects = Project.still_needs_some_work
```

## 38. Using Model Files to Create Database Column Default Values

just adding it to the model:

```rb
  after_initialize :set_defaults

  def set_defaults
    self.percent_complete ||= 0.0
  end
```

`||=`: Logic Or, it's basically conditional chaining;

The teacher removed the `percent_complete` from the form and then tested it to have the successfull response

Using `after_create` instead of `after_initialize` may be tempting, but wipes out values that we may or may not want to wipe.

## 39. How to Integrate Validations in Rails with Model Files

Just add the line to the models:

```rb
  validates_presence_of :title, :description
```

The rails docs has a lot of it, need to check it out

## 40. How to Generate a Model in Rails

First generate our model:

```
rails g model Task title:string description: text project:references
rake db:migrate
```

He explains all about the generated stuff, and the migration;
Using the `:references` does everything with the premisse of a foreign key

## 41. Setting Up Database Relations in a Model File

first fire up the console:

```
rails c
```

Then find the id of the last project:

```rb
Project.last
```

Use the command to add a task to it:

```rb
Task.create!(title: "My first task", description: "asdf", project_id: Project.last.id)
Task.create!(title: "My Second task", description: "asdf", project_id: Project.last.id)
```

To verify we can do:

```rb
Project.last.tasks
```

## 42. How to Add a New Column to a Database Using Migrations

```
rails g migration add_completed_to_tasks completed:boolean
rake db:migrate
```

## 43. Integrating Advanced Callbacks Using a Rails Model File

The Callback will verify if the task is saved, and will update the percent_complete of the project that it belongs to:

```rb
  after_save :update_percent_complete if :mark_completd?

  scope :completed, -> { where(completed: true) }

  def mark_completd?
    self.completed == true
  end

  def update_percent_complete
    project = Project.find(self.project_id)
    count_of_completed_tasks = project.tasks.completed.count
    count_of_total_tasks = project.tasks.count
    project.update!(percent_complete: Counter.calculate_percent_complete(count_of_completed_tasks, count_of_total_tasks))
  end
```

we can test it this way:

```rb
Project.last.percent_complete.to_s
# output would be "0.0"

Task.last.update!(completed: true)
# then we verify the percent_complete:
Project.last.percent_complete.to_s
# Output would be "50.0"
Task.last.update!(completed: false)
# then we verify the percent_complete again:
Project.last.percent_complete.to_s
# output back to "0.0"
```

Note that we didn't need to change the controller

## Test 7: Rails Models Quiz

1. At its core, what is a model file?
   R: A Ruby class
2. How do you generate a new model file (that inherits from ActiveRecord)?
   R: rails g model ModelName
3. What is NOT a function that a model should do?
   R: Hold view code
4. What validation method is used to ensure a database column is not blank?
   R: validates_presence_of
5. Can a callback be controlled by a conditional?
   R: Yes, you can add conditional logic

# Section 8 (6m): Rails Database Management

## 44. Review on Creating Columns in a Database Table

```
rails g migration add_stage_to_projects stage:integer
rake db:migrate
```

## 45. How to Change the Data Type of a Column in Rails

```
rails g migration change_data_type_for_stage
```

Then we go to the generated migration and change the file:

```rb
class ChangeDataTypeForStage < ActiveRecord::Migration[7.0]
  def change
    change_column :projects, :stage, :string
  end
end
```

then we migrate:

```
rake db:migrate
```

## 46. How to Remove a Column in Rails with a Migration

first add it:

```
rails g migration add_division_to_projects division:string
rake db:migrate
```

then delete it:

```
rails g migration remove_division_from_projects division:string
rake db:migrate
```

## Test 8: Rails Database Management Quiz

1. What is the syntax for adding a new column to a database table?
   R: rails g migration add_column_name_to_model column_name:data_type
2. What command do you run to run a database migration and update the schema file?
   R: rake db:migrate

# Section 9 (75m): Ruby Gems and Advanced Development Features

## 47. What are Ruby Gems?

We need to find a good balance.

- Some people don't use gems at all, which makes them spend too much time;
- Some use it too much, and spend more time finding the right gem than what would it take them to build it themselves.

Devi's gem for authentication;

Rails is a ruby gem;

## 48. How to Add Gems to an App's Gemfile

```gemfile
# file uploads
gem 'carrierwave'
# filters and autosizing
gem 'mini_magick', '~> 3.5.0'
# api connector between application and AWS
gem 'fog'
# credentials
gem 'figaro'
```

To install them, run in the console:

```
bundle install
```

Note: I had to set the version statically for fog:

```gemfile
gem 'fog', '~> 1.29.0'
```

## 49. Securing Application Credentials

```
figaro install
```

I had to change the file:
`/home/ze/.asdf/installs/ruby/3.2.2/lib/ruby/gems/3.2.0/gems/figaro-1.2.0/lib/figaro/cli/install.rb`
here:

```diff
      def ignore_configuration
-       if File.exists?(".gitignore")
+       if File.exist?(".gitignore")
          append_to_file(".gitignore", <<-EOF)
```

After that we add the credentials to the `application.yml`:

```yml
AWS_ACCESS_KEY_ID: "taski_user_taski_key"
AWS_SECRET_ACCESS_KEY: "taski_user_taski_key_secret"
development:
  AWS_BUCKET: my-taski-dev
production:
  AWS_BUCKET: my-taski-prd
```

## 50. How to Build Out a File Uploader in Rails with Carrierwave

First install `net-ssh` and `xmlrpc` by adding them to the Gemfile:

```gemfile
gem 'net-ssh'
gem 'xmlrpc'
```

then run the command to generate the uploader:

```
rails g uploader TaskFile
```

the file generated:

```rb
class TaskFileUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url(*args)
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process scale: [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process resize_to_fit: [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_whitelist
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end
end
```

but we'll change it to be this way:

```rb
class TaskFileUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick


  include Sprockets::Rails::Helper

  storage :fog

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_whitelist
    %w(jpg jpeg gif png pdf mp4 wmv xls xlsx doc docx ppt pptx mov)
  end

end
```

## 51. Connecting to the AWS API

Create the `initializers/fog.rb` file:

```rb
CarrierWave.configure do |config|
  config.fog_credentials = {
    :provider               => "AWS",
    :aws_access_key_id      => ENV["AWS_ACCESS_KEY_ID"],
    :aws_secret_access_key  => ENV["AWS_SECRET_ACCESS_KEY"]
  }

  config.fog_directory  = ENV["AWS_BUCKET"]
  config.fog_public     = false
end
```

## 52. How to Generate a Controller in Rails

First create the column `task_file`

```
rails g migration add_task_file_to_tasks task_file:text
rake db:migrate
```

Then create controller manually:

```
rails g controller tasks show new edit
```

It'll create the controller, and he explains the generated files

## 53. Manually Integrating CRUD Functionality Into a Rails Controller

Implemented the functions:

- `new`
- `create`
- `update`
- `destroy`

## 54. How to Add Private Methods to a Rails Controller

Implemented `set_task` and `task_params`

## 55. Creating Nested Routes in a Rails Application

First remove the task lines from the `routes.rb`:

```rb
  get 'tasks/show'
  get 'tasks/new'
  get 'tasks/edit'
```

we could just add resources here to avoid three lines:

```rb
  resources :tasks, except:[:index]
```

But we'll do it nested:

```rb
  resources :projects do
    resources :tasks, except:[:index], controller: 'projects/tasks'
  end
```

Then we move the `tasks_controller.rb` into a `controllers/projects/`

## 56. Setting Up a Nested Form in Rails

First move the `views/tasks` into `views/projects`

Add a new partial `_form.html.erb` and include the initial code:

```h
<%= form_for(@task) do |f| %>
  <%= if @task.errors.any? %>
    <%= pluralize(@task.errors.count, "error") %> prohibited this project from being saved:

    <%= @task.errors.full_messages.each do |message| %>
      <%= message %>
    <%= end %>
  <%= end %>
  <%= f.label :title %>
  <%= f.text_field :title %>

  <%= f.label :description %>
  <%= f.text_area :description %>

  <%= f.label :completed %>
  <%= f.check_box :completed %>

  <%= f.label "Attachment" %>
  <%= f.file_field :task_file %>

  <%= f.submit %>

<%= end %>
```

## 57. Integrating Parent Values in a Nested Resource Controller Method

First we change the class definition:

```diff
- class TasksController < ApplicationController
+ class Projects::TasksController < ApplicationController
```

We create the `set_project` private function on `tasks_controller.rb`:

```
    def set_project
      @project = Project.find params[:project_id]
    end
```

we do a before action too:

```
  before_action :set_project, only: [:show, :new, :edit, :create, :update, :destroy]
```

And we also set the project_id in the `create` method:

```diff
def create
    @task = Task.new(task_params)
+   @task.project_id = @project.id

    respond_to do |format|
```

## 58. Configuring Custom Redirects for Nested Resources

I had to change the following file:

- `~/.asdf/installs/ruby/3.2.2/lib/ruby/gems/3.2.0/gems/fog-1.29.0/lib/fog/xenserver/utilities.rb`

the following way:

```diff
class Hash
  def symbolize_keys!
    keys.each do |key|
-     self[(key.to_sym rescue key)] = delete(key) if key.respond_to?(:to_sym) && !key.is_a?(Fixnum)
+     self[(key.to_sym rescue key)] = delete(key) if key.respond_to?(:to_sym) && !key.is_a?(Integer)
    end
    self
  end
end
```

Here we basically added the following text in the first part of format.html of some task controller methods:

```rb
redirect_to project_url(@task.project_id)
```

## 59. Integrating Arguments Into Links in Rails

This will give us all the routes with the `project` text in the line

```
rails routes | grep project
```

First get the route for new_project_task:

```
new_project_task GET /projects/:project_id/tasks/new(.:format) projects/tasks#new
```

In `projects/show.html.erb`, we added this:

```h
<h2> Tasks for project</h2>

<%= link_to "Add a new task for this project", new_project_task_path(project_id: @project.id) %>
```

you can see that we grabbed the path from the `rake` command and just appended the `path` name to it

## 60. Integrating Multiple Queries on a Single Page in Rails

We could do this on the `projects/show.html.erb`:

```
<%= @project.tasks.each do |task| %>
```

BUT WE WON'T DO IT because is not rubyan, the thing is:

> You will want little to no logic going on in the view itself. You'll wanna control this either from the model or the controller
> Add in projects_controller

First we get the task show page from the `rails routes | grep task` command:

```
project_task GET /projects/:project_id/tasks/:id(.:format) projects/tasks#show
```

And with that we mount the `project_task_path` call which needs a `project_id` and a `task_id`:

```h
<% @tasks.each do |task| %>
  <p><%= link_to task.title, project_task_path(@project.id, task.id) %></p>
<% end %>
```

## 61. Integrating Conditionals Into View Pages for Custom Page Behavior

Included all the logic for the `tasks/show.html.erb` file:

```h
<h1>Tasks#show</h1>

<h3>
  <%= @task.title %>
</h3>

<p><%= @task.description %></p>

<h4> Task started: <%= @task.created_at %></h4>

<% if @task.created_at != @task.updated_at %>
  <h4> Updated: <%= @task.updated_at %></h4>
<% end %>

<% if @task.completed == true %>
<p><em>Task already completed</em></p>
<% else %>
<p><em>Pending Task</em></p>
<% end %>

<% unless @task_file.blank? %>
  <p><%= link_to "Attachment", @task.task_file.to_s %></p>
<% end %>

   text
   completed
   task_file

<%= link_to "Edit", edit_project_task_path(@task.project_id, @task.id) %>
<%= link_to "Back to Project", project_path(@task.project_id) %>
```

## 62. Finalizing File Uploads and Performing Advanced Debugging

First insert the line in the `task.rb` file:

```rb
  mount_uploader :task_file, TaskFileUploader
```

At first, it didn't work in my system.
Then I realized that instead of `TaskFileUploader`, my uploader had the name `TestFileUploader`, then it uploaded.
But after that we had other problem: it was saving in my local storage.
After some consideration we realized that we created a `task_file_uploader.rb` and didn't placed the right code.
After performing the last correction, the upload into my s3 happened successfully.

In the end of the class, the teacher advised to take a look into the terminal for any debugging that may be needed. We can even print some text with:

```rb
  puts "*" * 500
  puts "beggining of callback"
  ...
  puts "*" * 500
  puts "end of callback"
```

## Test 9: Ruby Gems and Advanced Development Features Quiz

1. What is a Ruby Gem?
   R: A Ruby code Library
2. Where is the Gemfile for your app located?
   R: At the root of the application
3. What command is used to generate the file for securing the app's config variables?
   R: figaro install
4. What gem is used for managing file uploads?
   R: Carrierwave
5. How do you run a controller generator?
   R: rails g controller ControllerName
6. Is it possible to create custom controller methods manually?
   R: Yes, anything a generator can do I can do better
7. What is something you do not need to do in order to setup a nested resource?
   R: Move the model file into a nested folder named after the parent resource

# Section 10 (22m): Authentication

## 63. Installing the Devise Gem for Authentication

First insert into `Gemfile`:

```
gem 'devise'
```

then install it:

```
bundle install
```

Then you can generate stuff:

```
rails generate devise:install
```

Inside the generated `devise.rb` file, changed the `config.mailer_sender`:

```rb
  config.mailer_sender = 'jmsj777@hotmail.com'
  # just added this email but probably won't see it
```

After that, we create the `_alerts` shared view and call it in `application` view, with the `notice` and `alert`;

To finish the setup, we do the command to generate the views:

```
rails g devise:views
```

## 64. Creating a Devise User Model in Rails

First create a user model on the database

```
rails g devise User
rake db:migrate
```

## 65. Testing Registration and Signing Into a Rails Application in the Browser

First signup:

```
http://127.0.0.1:3000/users/sign_up
```

then signin:

```
http://127.0.0.1:3000/users/sign_in
```

## 66. Integrating the current_user Method into a Rails App

Between a lot of stuff, we did the following:

```h
<% if current_user %>
  <%= button_to "Sign Out", destroy_user_session_path, method: :delete %>
<% else %>
  <%= link_to "Register", new_user_registration_path %>
  <%= link_to "Sign In", new_user_session_path %>
<% end %>
```

Note that we changed from `link_to` to `button_to` in the signout route, because the `link_to` was not giving a `DELETE` method, just a GET

## 67. How to Allow User to Edit Account Info in Rails

Below the button to sign out, just add the edit link:

```h
  <%= link_to "Edit Account", edit_user_registration_path %>
```

Finished by showing us the locale file and when we would change it.

## Test 10: Authentication Quiz

1. What Gem is used for authentication?
   R: Devise
2. What command is used to generate a Devise User model?
   R: rails generate devise User
3. What route is used to access the registration page locally?
   R: localhost:3000/users/sign_up
4. What does the current_user method give the application the ability to do?
   R: To know if a user is signed into the application

# Section 11 (32m): Deploying a Rails Application

## 68. Overview of Rails Deployment Options

Best one is heroku

Other options:

- Engine Yard;
- Rails Machine;
- DigitalOcean;
- Linode;
- AWS Beanstalk;
- AWS EC2.

## 69. Heroku Deployment Requirements

- Create a local git repository
- Create your initial commit
- Create a remote repository on GitHub
- Push your local code to the remote repository
- Sign up for a Heroku account
- Install the Heroku CLI toolbelt: https://toolbelt.heroku.com/
- Create the application on Heroku
- Integrate Postgres for the production database
- Integrate config variables on Heroku
- Push your master branch from GitHub to your Heroku application
- Run rake db:migrate

## 70. Integrating git to a Rails Application and Pushing to GitHub

Put the code into the github repo;

## 71. Deploying to Heroku

I've done this before for other projects, so I won't do, but here's what he did:

```bash
git remote -v
# it will show the local and origin;
heroku create edutechional-taski
git remote -v
# now it will show the ones before plus the git.heroku ones

git push heroku master
```

It'll deploy for us
The teacher had a problem about sqlite, and that's why he included the following instruction on the previous chapter:

> Integrate Postgres for the production database

In the `Gemfile`:

```diff
- gem "sqlite3", "~> 1.4"
+ gem "pg"
```

also, ran into a problem about config:

```bash
heroku config:set AWS_ACCESS_KEY_ID="taski_user_taski_key"
heroku config:set AWS_SECRET_ACCESS_KEY="taski_user_taski_key_secret"
heroku config:set AWS_BUCKET="my-taski-dev"
```

After that, do the `add > commit > push` flow for git;
then do `git push heroku master` again;

lastly, do:

```
heroku run rake db:migrate
```

The bash shows us a link to access the test.
After some seconds of cold start, it works, and he creates user, project and tasks

## 72. Accessing the Rails Console on Heroku

```
heroku run rails c
```

now it's just like the normal rails console

## Test 11: Rails Deployment Quiz

1. Which of the options below is not a Rails deployment option?
   R: Yahoo!
2. What command do you use to create a git repository for an application?
   R: git init
3. What types of GitHub accounts are free?
   R: Open source projects
4. What command is used to deploy to Heroku?
   R: git push heroku master
5. What would not cause Heroku to reject an application?
   R: Too much code in the controller
6. What can you do to access the production database?
   R: heroku run rails c

# Section 12 (2m): Summary

## 73. Course Summary

Must review all code, and study more materials too. Traversy media has a more updated crash course, may work on that later

## Test 12: Final Quiz

1. What do each of the words after a controller generator represent? Example: rails g controller Page home contact faqs
   R: They will create a view, controller method, and route
2. What gem was used for securing API credentials?
   R: Figaro
3. When will you need to restart the Rails server to see code changes?
   R: After changes to any files except in the 'app/' directory
4. What is the syntax for ERB in a view file?
   R: <%= %>
5. What type of architecture is the Rails framework?
   R: Model View Controller
6. What command do you run in the terminal to install gems after you've made a change to the Gemfile
   R: bundle install
7. What method does Devise give to let you know if a user is signed in or not?
   R: current_user
8. What callback is best to use for configuring a default value in a model file?
   R: after_initialize
9. What type of routing design does Rails utilize?
   R: RESTful
10. What is one of the main Rails controller antipatterns?
    R: Putting too much logic in the controller final instead of the model.
