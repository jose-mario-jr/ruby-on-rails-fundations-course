class Task < ApplicationRecord
  belongs_to :project

  # callback will verify if the task is saved, and will updte the
  # percent_complete of the project that it belongs to
  after_save :update_percent_complete if :mark_completd?

  scope :completed, -> { where(completed: true) }

  mount_uploader :task_file, TaskFileUploader

  def mark_completd?
    self.completed == true
  end

  def update_percent_complete
    project = Project.find(self.project_id)
    count_of_completed_tasks = project.tasks.completed.count
    count_of_total_tasks = project.tasks.count
    project.update!(percent_complete: Counter.calculate_percent_complete(count_of_completed_tasks, count_of_total_tasks))
  end
end
