require "test_helper"

class Projects::TasksControllerTest < ActionDispatch::IntegrationTest
  # test "should get show" do
  #   get project_task_url
  #   assert_response :success
  # end

  # test "should get new" do
  #   get new_project_task_url
  #   assert_response :success
  # end

  # test "should get edit" do
  #   get edit_project_task_url
  #   assert_response :success
  # end

  # test "should create project task" do
  #   assert_difference("Project.tasks.count") do
  #     post project_tasks, params: { project: { description: @project.description, percent_complete: @project.percent_complete, title: @project.title, tasks: @tasks } }
  #   end

  #   assert_redirected_to project_url(Project.last)
  # end

  test "should get project_tasks" do
    project = Project.last
    get project_tasks_url(project)
    assert_response :success
  end

end
